
library ieee;
use ieee.std_logic_1164.all;

entity authorization is
   port(
      keypad: in std_logic_vector(3 downto 0) := x"0";
      enter: in std_logic := '1';
      passChange: in std_logic := '1';
      clk: in std_logic;
      authSig: out std_logic := '0';
      passLED: out std_logic := '0');
end authorization;

architecture rtl of authorization is
   signal password: std_logic_vector(3 downto 0) := x"0";
   signal passLock: boolean := true;
begin

   process(enter)
   begin
		if (password xor keypad) = x"0" and (enter = '0') then
			authSig <= '1';
		elsif (enter = '1') then
			authSig <= '0';
		end if;
   end process;

   process(passChange, clk)
      variable lock: bit := '1';
   begin
      if rising_edge(clk) then
         if (lock = '1') and (password xor keypad) = x"0" and (passChange = '0') then
            lock := '0';
            passLED <= '1';
         elsif (lock = '0') and (passChange = '0') then
            for i in password'range loop
               password(i) <= keypad(i);
            end loop;
            lock := '1';
            passLED <= '0';
         end if;
      end if;
   end process;
end rtl;