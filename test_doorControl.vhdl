library ieee;
use ieee.std_logic_1164.all;

entity test_doorControl is
end test_doorControl;

architecture rtl of test_doorControl is
-- Variables
   signal doorSignal : std_logic;
   signal mainclk: std_logic;
   signal doorLED: std_logic;

--Components
   component doorControl port(
      sig: in std_logic := '0';
      state : out std_logic := '0';
      clk : in std_logic);
   end component;

-- BEGIN ARCHITECTURE
begin
-- Init
   doorCTRL : doorControl port map(
      sig => doorSignal,
      clk => mainclk,
      state => doorLED);

-- Test Bed Process
   TestBed : process
   begin
      report "##### BEGIN DOOR CONTROL TESTBENCH #####";
      -- Test #1: Default Startup State
      assert doorLED = '0' report "Door State failed on Test #1" severity failure;

      -- Test #2: Short press door button to activate
      doorSignal <= '1';
      wait for 20 ns;
      doorSignal <= '0';
      wait for 25 ns;
      assert doorLED = '1' report "Door State failed on Test #2" severity failure;

      -- Test #3: Make sure signal remains in its state
      wait for 100 ns;
      assert doorLED = '1' report "Door State failed on Test #3" severity failure;

      -- Test #4: Short press button to deactivate
      doorSignal <= '1';
      wait for 20 ns;
      doorSignal <= '0';
      wait for 25 ns;
      assert doorLED = '0' report "Door State failed on Test #4" severity failure;

      -- Test #5: Long press door button to activate
      doorSignal <= '1';
      wait for 25 ns;
      assert doorLED = '1' report "Door State failed on Test #5" severity failure;
      doorSignal <= '0';
      wait for 25 ns;

      -- Test #6: Make sure signal remains in its state
      wait for 100 ns;
      assert doorLED = '1' report "Door State failed on Test #6" severity failure;

      -- Test #7: Long press button to deactivate
      doorSignal <= '1';
      wait for 25 ns;
      assert doorLED = '0' report "Door State failed on Test #7" severity failure;
      doorSignal <= '0';
      wait for 25 ns;

      -- Test #8: Make sure signal remains in its state
      wait for 100 ns;
      assert doorLED = '0' report "Door State failed on Test #6" severity failure;

      report "DOOR CONTROL TESTBENCH PASSED";
      wait;
   end process;

--   Clock Process
   CLK : process
   begin
      mainclk <= '0';
      wait for 2 ns;
      mainclk <= '1';
      wait for 2 ns;
   end process;

end rtl;