library ieee;
use ieee.std_logic_1164.all;

entity adder3 is
   port (
      A : in std_logic_vector(2 downto 0);
      B : in std_logic_vector(2 downto 0);
      cryi : in std_logic := '0';
      sum : out std_logic_vector(2 downto 0);
      cryo : out std_logic);
end adder3;

architecture rtl of adder3 is
   signal c0, c1: std_logic;
-- COMPONENTS
   component adder port (
      i0, i1 : in std_logic;
      ci : in std_logic;
      s : out std_logic;
      co : out std_logic);
   end component;
begin
   adderb0 : adder port map(
      i0 => A(0),
      i1 => B(0),
      ci => cryi,
      s  => sum(0),
      co => c0);

   adderb1 : adder port map(
      i0 => A(1),
      i1 => B(1),
      ci => c0,
      s  => sum(1),
      co => c1);

   adderb2 : adder port map(
      i0 => A(2),
      i1 => B(2),
      ci => c1,
      s  => sum(2),
      co => cryo);
end rtl;