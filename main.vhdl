
library ieee;
use ieee.std_logic_1164.all;

entity main is
   port (
      mainclk: in std_logic;
      doorLED: out std_logic;
      passLED: out std_logic;
      enterButton: in std_logic := '1';
      passButton: in std_logic := '1';
      switches: in std_logic_vector(3 downto 0) := x"0");
end main;

architecture rtl of main is
   signal doorSignal : std_logic;
   component doorControl port(
      sig: in std_logic;
      state : out std_logic := '0';
      clk : in std_logic);
   end component;
   component authorization port(
      keypad: in std_logic_vector(3 downto 0) := x"0";
      enter: in std_logic := '0';
      passChange: in std_logic := '0';
      authSig: out std_logic := '0';
      clk: in std_logic;
      passLED: out std_logic := '0');
   end component;
begin
   doorCTRL : doorControl port map(
      sig => doorSignal,
      clk => mainclk,
      state => doorLED);

   auth : authorization port map(
      keypad => switches,
      enter => enterButton,
      passChange =>  passButton,
      authSig => doorSignal,
      clk => mainclk,
      passLED => passLED);
end rtl;