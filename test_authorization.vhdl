library ieee;
use ieee.std_logic_1164.all;

entity test_authorization is
end test_authorization;

architecture rtl of test_authorization is
-- Variables
   signal authState: std_logic;
   signal passLED: std_logic;
   signal enterButton: std_logic;
   signal passButton: std_logic;
   signal switches: std_logic_vector(3 downto 0);

--Components
   component authorization port(
      keypad: in std_logic_vector(3 downto 0) := x"0";
      enter: in std_logic := '0';
      passChange: in std_logic := '0';
      authSig: inout std_logic := '0';
      passLED: out std_logic := '0');
   end component;

-- BEGIN ARCHITECTURE
begin
-- Init
   auth: authorization port map(
      keypad => switches,
      enter => enterButton,
      passChange => passButton,
      authSig => authState,
      passLED => passLED);

-- Test Bed Process
   TestBed : process
   begin
      report "##### BEGIN AUTHORIZATION TESTBENCH #####";
      -- Test #1: Default Startup State
      switches <= x"0";
      enterButton <= '0';
      passButton  <= '0';
      wait for 20 ns;
      assert authState = '0' report "Auth State failed on Test #1" severity failure;
      assert passLED = '0' report "Pass LED failed on Test #1" severity failure;

      -- Test #2: Test Default Password 
      switches <= x"0";
      enterButton <= '1';
      passButton  <= '0';
      wait for 20 ns;
      enterButton <= '0';
      assert authState = '1' report "Auth State failed on Test #2" severity failure;
      assert passLED = '0' report "Pass LED failed on Test #2" severity failure;
      
      -- Test #3: Test Default Password (hold button)
      switches <= x"0";
      enterButton <= '1';
      passButton  <= '0';
      wait for 20 ns;
      assert authState = '1' report "Auth State failed on Test #3" severity failure;
      assert passLED = '0' report "Pass LED failed on Test #3" severity failure;
      enterButton <= '0';
      wait for 20 ns;

      -- Test #4: Test Change Password Button (Start Password Change)
      switches <= x"0";
      enterButton <= '0';
      passButton  <= '1';
      wait for 20 ns;
      passButton  <= '0';
      wait for 20 ns;
      assert authState = '0' report "Auth State failed on Test #4" severity failure;
      assert passLED = '1' report "Pass LED failed on Test #4" severity failure;

      -- Test #5: Change the password (Finish password change)
      switches <= x"4";
      enterButton <= '0';
      passButton  <= '1';
      wait for 20 ns;
      passButton  <= '0';
      wait for 20 ns;
      assert authState = '0' report "Auth State failed on Test #5" severity failure;
      assert passLED = '0' report "Pass LED failed on Test #5" severity failure;

      -- Test #6: Test new Password
      switches <= x"4";
      enterButton <= '1';
      passButton  <= '0';
      wait for 20 ns;
      enterButton <= '0';
      assert authState = '1' report "Auth State failed on Test #6" severity failure;
      assert passLED = '0' report "Pass LED failed on Test #6" severity failure;

      -- Test #7: Test bad password
      switches <= x"2";
      enterButton <= '1';
      passButton  <= '0';
      wait for 20 ns;
      enterButton <= '0';
      assert authState = '0' report "Auth State failed on Test #7" severity failure;
      assert passLED = '0' report "Pass LED failed on Test #7" severity failure;

      report "AUTHORIZATION TESTBENCH PASSED";
      wait;
   end process;
end rtl;