library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.ALL;

entity bcd is
   port (
      bin : in std_logic_vector(5 downto 0);
      seg7A : out std_logic_vector(6 downto 0);
      seg7B : out std_logic_vector(6 downto 0));
end bcd;

architecture rtl of bcd is
begin
   process(bin)
      variable decVal: integer;
      variable d1, d2: integer;
   begin
      decVal := to_integer(unsigned(bin));
      d1 := decVal / 10;
      d2 := decVal mod 10;
      if (d1 = 0) then
      end if;

      case d1 is
         when 0 => seg7A <= "1111110";  -- '0'
         when 1 => seg7A <= "0110000";  -- '1'
         when 2 => seg7A <= "1101101";  -- '2'
         when 3 => seg7A <= "1111001";  -- '3'
         when 4 => seg7A <= "0110001";  -- '4' 
         when 5 => seg7A <= "1011010";  -- '5'
         when 6 => seg7A <= "1011111";  -- '6'
         when 7 => seg7A <= "1110000";  -- '7'
         when 8 => seg7A <= "1111111";  -- '8'
         when 9 => seg7A <= "1110011";  -- '9'
         when others => seg7A <= "0000000"; 
      end case;

      case d2 is
         when 0 => seg7B <= "1111110";  -- '0'
         when 1 => seg7B <= "0110000";  -- '1'
         when 2 => seg7B <= "1101101";  -- '2'
         when 3 => seg7B <= "1111001";  -- '3'
         when 4 => seg7B <= "0110001";  -- '4' 
         when 5 => seg7B <= "1011010";  -- '5'
         when 6 => seg7B <= "1011111";  -- '6'
         when 7 => seg7B <= "1110000";  -- '7'
         when 8 => seg7B <= "1111111";  -- '8'
         when 9 => seg7B <= "1110011";  -- '9'
         when others => seg7B <= "0000000"; 
      end case;
   end process;
end rtl;