
library ieee;
use ieee.std_logic_1164.all;

entity doorControl is
   port(
      sig: in std_logic;
      state : out std_logic := '0';
      clk : in std_logic);
end doorControl;

architecture rtl of doorControl is
begin
   process(clk, sig)
		variable lock : boolean;
		variable internState: std_logic := '0';
   begin
		if rising_edge(clk) then
			if (sig = '0') then
				lock := false;
			elsif(not lock) and (sig = '1') then
				internState := not internState;
				state <= internState;
				lock := true;
			end if;
		end if;
   end process;
end rtl;